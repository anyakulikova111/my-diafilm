#!make

#* Variables
SHELL := /usr/bin/env bash
PYTHON := python

#* Docker variables
PROJECT := diafilms
IMAGE := registry.gitlab.com/toptalo/diafilm/backend
VERSION := latest

#* Custom variables
CMD := help

#* Custom shit
.PHONY: help
help: ## show this message
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[$$()% a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

.PHONY: requirements
requirements: ## build requirements file
	PIP_PREFER_BINARY=1 pip-compile --quiet \
			    --output-file requirements/base.txt \
			    requirements/base.in


.PHONY: lint
lint: ## remove unused imports and sort other
	autoflake --in-place --remove-all-unused-imports ./**/*.py
	isort .

.PHONY: build
build: ## build image with VERSION=x.y.z
	DOCKER_BUILDKIT=1 docker compose build --pull backend --build-arg VERSION=$(version)

.PHONY: start
start: ## up containers
	docker compose pull
	docker compose -p ${PROJECT} up -d --wait

.PHONY: up
up: ## start alias
	make start

.PHONY: stop
stop: ## down containers
	docker compose -p ${PROJECT} down

.PHONY: down
down: ## stop alias
	make stop

.PHONY: logs
logs: ## watch backend logs
	docker compose -p ${PROJECT} logs backend -f

.PHONY: exec
exec: ## bash into backend
	docker compose -p ${PROJECT} exec backend bash

.PHONY: healthcheck
healthcheck: ## healthcheck URL=HOST:PORT
	python deploy/healthcheck.py ${URL}
