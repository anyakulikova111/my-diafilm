import sys
import urllib.request
from urllib.error import URLError


if __name__ == "__main__":
    url = "http://{}/healthcheck/".format(
        sys.argv[1]
        if len(sys.argv) > 1 else
        "127.0.0.1:8000"
    )

    try:
        print(f"Checking {url} ", end="")
        response = urllib.request.urlopen(url, timeout=1)
    except URLError:
        response = None

    if not response or response.status != 200:
        print("FAIL")
        exit(1)

    print("OK")
